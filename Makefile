SHELL = /bin/bash

build_tag ?= dspace

.PHONY: build
build:
	docker build -t $(build_tag) -f Dockerfile.gitlab .

.PHONY: test
test:
	docker run $(build_tag) mvn test -DskipUnitTests=false
