package dukespace.embargo;

import org.dspace.embargo.DefaultEmbargoSetter;
import org.dspace.authorize.AuthorizeException;
import org.dspace.content.Item;
import org.dspace.core.Context;
import org.dspace.content.DCDate;
import org.dspace.services.factory.DSpaceServicesFactory;

import java.sql.SQLException;
import java.util.Calendar;


public class MonthEmbargoSetter extends DefaultEmbargoSetter {
  public MonthEmbargoSetter() {
    super();
  }

  /**
   * Parse the terms into a definite date. Terms are given in integer months.
   *
   * @param context the DSpace context
   * @param item    the item to embargo
   * @param terms   the embargo terms
   * @return parsed date in DCDate format
   */
  @Override
  public DCDate parseTerms(Context context, Item item, String terms) throws SQLException, AuthorizeException {
    String termsMax = DSpaceServicesFactory.getInstance().getConfigurationService()
        .getProperty("embargo.terms.max");
    int max;
    try {
      max = Integer.parseInt(termsMax);
    } catch (NumberFormatException e) {
      max = 24;
    }
    if (terms != null && terms.length() > 0 ) {
      try {
        int m = Integer.parseInt(terms);
        if (m > max) {
          m = max;
        }
        if (m <= 0) {
          return null;
        }
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.MONTH, m);
        return new DCDate(cal.getTime());
      } catch (NumberFormatException e) {
        return null;
      }
    }
    return null;
  }

}
