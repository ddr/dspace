#!/bin/bash

AIP_FILENAME="$(basename $(find sample-data -maxdepth 1 \( -name '*.zip' -o -name '*.zip.bak' \) | head -1) .bak)"
if [ $AIP_FILENAME == ".bak" ]; then
  echo "No .zip or .zip.bak file found in sample-data/ directory, please download a sample AIP ingest file and place it in sample-data/ before running this script."
  exit 1
else
  echo "Using ${AIP_FILENAME} as ingest filename"
fi

echo "Checking if DSpace is running..."
if ! docker-compose -f docker-compose.yml -p d7 ps dspace --status running | fgrep dspace > /dev/null; then
  echo "DSpace must already be running for the initial setup script:"
  echo "  docker-compose -f docker-compose.yml -p d7 up -d"
  exit 1
fi

if [ -e "sample-data/${AIP_FILENAME}" ]; then
  echo "Running initial DukeSpace setup..."
  echo "Creating administrator account..."
  docker-compose -p d7 -f docker-compose-cli.yml run --rm dspace-cli create-administrator -e test@test.edu -f admin -l user -p admin -c en
  echo "Registering duke-types.xml..."
  docker-compose -p d7 -f docker-compose-cli.yml run --rm dspace-cli registry-loader -metadata dspace/config/registries/duke-types.xml
  echo "Registering pubs-types.xml..."
  docker-compose -p d7 -f docker-compose-cli.yml run --rm dspace-cli registry-loader -metadata dspace/config/registries/pubs-types.xml
  echo "Ingesting sample data..."
  docker-compose -p d7 -f docker-compose-cli.yml -f dspace/src/main/docker-compose/cli.ingest-duke-sample-data.yml run --rm dspace-cli
  if [ $? -eq 0 ]; then
    mv -v "sample-data/${AIP_FILENAME}" "sample-data/${AIP_FILENAME}.bak"
    echo "Initial DukeSpace setup complete. ${AIP_FILENAME} renamed to ${AIP_FILENAME}.bak to prevent multiple accidental runs. To re-run, simply rename back to ${AIP_FILENAME}."
  else
    echo "Sample data ingest exited with error. Please check the logs."
    exit 1
  fi
else
  if [ -e "sample-data/${AIP_FILENAME}.bak" ]; then
    echo -n "Rename ${AIP_FILENAME}.bak to ${AIP_FILENAME}"
  else
    echo -n "Place ${AIP_FILENAME}"
  fi
  echo " in the sample-data/ directory before running initial setup."
  exit 1
fi
