# Sample Data Directory (DUL CUSTOMIZATION)

This `sample-data` folder is mounted in the `dspace` container for use with Docker in a local development environment.

Authorized DUL staff may download a [sample data ZIP file from Box](https://duke.app.box.com/folder/169061340723) and copy it to this directory for command-line ingest. Make sure the ZIP filename ends in `.zip` and doesn't include spaces.

## Notes About Sample Data
* A wide range of about 50 actual DukeSpace items and their bitstreams, from 10-15 collections/communities.
* A ZIP file in [DSpace AIP format](https://wiki.lyrasis.org/display/DSDOC6x/AIP+Backup+and+Restore#AIPBackupandRestore-ExportingAIPs)
* Represents many item types with various characteristics.
* Under 100MB
